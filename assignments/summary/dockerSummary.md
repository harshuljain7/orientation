# DOCKER
![DOCKER](https://miro.medium.com/max/875/1*JUOITpaBdlrMP9D__-K5Fw.png)
>Docker is a program for developers to develop, and run applications with containers.
>
**_WHY DOCKER_**
>It becomes hard to manage dependencies as we build large projects and if we want to deploy them in various environments
>
_**DOCKER TERMINOLOGIES**_  
![DT](https://miro.medium.com/max/875/1*9uNw2h81iTCEKzy6kvNRKQ.png)
>1.**Docker Image** - A Docker image is contains everything needed to run an applications as a container such as code,runtime,libraries,eniviroment,files.The image can then be deployed to any Docker environment and as a container  
>2.**Container** -Running Image is Container. If Image is a class then the container is an instance   
>3.**Hub** - Docker Hub is like GitHub but for docker images and containers  
>4.**Registry**- Redistribution is done using the Registry(ex: https://hub.docker.com).
>
_**DOCKER ENGINE**_  
![DE](https://miro.medium.com/max/680/1*xzcKCrnHGFJqUbHVsgfTmw.png)  
_**BASIC COMMANDS**_
>* **docker ps**- The docker ps command allows us to view all the containers that are running on the Docker Host.
>* **docker start**- This command starts any stopped container(s).
>* **docker stop**- This command stops any running container(s
>* **docker run**- This command creates containers from docker images.
>* **docker rm**- This command deletes the containers.
>
_**WORKFLOW**_  
![DF](https://raw.githubusercontent.com/rossbachp/docker-basics/master/images/docker-command-flow.png)  
>1.Download/pull the docker images that you want to work with.  
>2.Copy your code inside the docker  
>3.Access docker terminal   
>4.Install and additional required dependencies  
>5.Compile and Run the Code inside docker  
>6. Document steps to run your program in README.md file  
>7.Commit the changes done to the docker  
>8.Push docker image to the docker-hub and share repository with people who want to try your code.  
