# GIT
![git](https://avatars3.githubusercontent.com/u/18133?s=200&v=4)  
_**WHAT IS GIT**_  
>Git is a type of version control system  that makes it easier to track changes to files.     
It enables us to manage  every change, development or modification in the code by them or their teammates. 
>   
_**VOCABULARY NEEDED FOR GIT**_   
>* **Repository** - Collection of files and folders.
>* **Commit** - Saving our work by taking Current snapshot of repository with changes
>* **GitLab** - Remote storage solution for git repos.
>* **Push** - Pushing is saving and syncing our saved work or commits
>* **Fork** - Repository Copy Under your username
>* **Clone** - Cloning or Copying of code into our machine
>* **Branch** -Software is divided into codes known as branches with is later merged with main Software
>* **Merge** - Integration of the branches with main code.
>
_**GIT WORKFLOW**_  
![WORKFLOW](https://res.cloudinary.com/practicaldev/image/fetch/s--M_fHUEqA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/128hsgntnsu9bww0y8sz.png)  
>1.Clone - _git clone (link to repository)_  
>2.Creating New Branch -git checkout_  
>3.Staging Changes/Modifying Files -_ git add_  
>4.Commit to Files -_ git commit_  
>5.Pushing The File -_git push branch name_  
>
_**FILE STATUS**_  
![STATUS](https://i.stack.imgur.com/QaeAZ.png)
_**GIT INTERNALS**_
>3 Stages
>* **Modified** means that you have changed the file but have not committed it to your repo yet.
>* **Staged means** that you have marked a modified file in its current version to go into your next picture/snapshot.
>* **Committed** means that the data is safely stored in your local repo in form of pictures/snapshots.
